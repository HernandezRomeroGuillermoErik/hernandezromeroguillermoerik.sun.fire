<?php foreach ($datos['autores'] as $autor) {?>
<form method="post" action="../libros/principal.php?c=libros&amp;a=editar_autor" class="form-horizontal" role="form">
	<div class="form-group">
		<label for="id_autor" class="col-md-4">Id autor:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $autor['id_autor']?>"
			id="id_libro" />
		</label
	></div>        
    <div class="form-group">
		<label for="nombre_autor" class="col-md-4">Nombre del autor:
            <input type="text" 
            class="form-control col-md-8"
			name="libro[titulo_libro]"
			value="<?php echo $autor['nombre_autor']?>"
			id="titulo_libro" />
		</label>
	</div>
    	<div class="form-group">
		<label for="nacionalidad_autor" class="col-md-4">Nacionalidad del autor:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $autor['nacionalidad_autor']?>"
			id="id_libro" />
		</label>
	</div>     


	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Modificar</button>
        </div>
	</div>
</form>
<?php } ?>
<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>