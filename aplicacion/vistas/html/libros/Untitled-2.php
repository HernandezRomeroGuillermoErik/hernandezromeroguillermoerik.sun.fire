<?php foreach ($datos['libros'] as $libro) {;?>
<form method="post" action="principal.php?c=libros&a=editar_libro" class="form-horizontal" role="form">
	<div class="form-group">
		<label for="id_libro" class="col-md-4">Id del libro:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $libro['id_libro']?>"
			id="id_libro" />
		</label>
	</div>        
    <div class="form-group">
		<label for="titulo_libro" class="col-md-4">ISBN del libro:
            <input type="text" 
            class="form-control col-md-8"
			name="libro[titulo_libro]"
			value="<?php echo $libro['isbn_libro']?>"
			id="titulo_libro" />
		</label>
	</div>
    	<div class="form-group">
		<label for="id_libro" class="col-md-4">Titulo del libro:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $libro['titulo_libro']?>"
			id="id_libro" />
		</label>
	</div>     
    	<div class="form-group">
		<label for="id_libro" class="col-md-4">Editorial del libro:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $libro['editorial_libro']?>"
			id="id_libro" />
		</label>
	</div>    
    <div class="form-group">
		<label for="id_libro" class="col-md-4">Año de publicacion:
            <input type="text"
			class="form-control col-md-8"
			name="libro[id_libro]"
			value="<?php echo $libro['anio_publicacion_libro']?>"
			id="id_libro" />
		</label>
	</div>     

	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Modificar</button>
        </div>
	</div>
</form>
<?php } ?>
<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>