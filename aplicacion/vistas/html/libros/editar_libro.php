
<form method="post" action="principal.php?c=libros&a=guardar_libro"
	class="form-horizontal" role="form">

	<div class="form-group">
		<label for="titulo_libro" class="col-md-4">titulo del libro:
            <input type="text" placeholder="Ingresa el titulo"
            class="form-control col-md-8"
			name="libro[titulo_libro]"
			value="<?php echo @$datos[libro]['titulo_libro']; ?>"
			id="titulo_libro" />
		</label>
	</div>

	<div class="form-group">
		<label for="isbn_libro" class="col-md-4">isbn del libro:
            <input type="text" placeholder="ingresa la isbn"
			class="form-control col-md-8"
			name="libro[isbn_libro]"
			value="<?php echo @$datos[libro]['isbn_libro']; ?>"
			id="isbn_libro" />
		</label>
	</div>

	<div class="form-group">
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Editar</button>
        </div>
	</div>

</form>

<?php if (@$datos['error'] == true) { ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading">Errores</div>
			<div class="panel-body">
			<ul>
                <?php foreach (@$datos['mensajes_error'] as $error) { ?>
                <li><?php echo $error; ?></li>
                <?php } ?>
			</ul>
			</div>
		</div>
	</div>
</div>
<?php } ?>
