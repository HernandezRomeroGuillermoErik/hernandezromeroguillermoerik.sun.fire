
<div class="navbar navbar-inverse" role="navigation">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="sr-only">Biblioteca</span>
				<span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="principal.php">Biblioteca</a>
		</div>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Autores<b class="caret"></b></a>
					<ul class="dropdown-menu">
						
						<li><a href="principal.php?c=autores&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="principal.php?c=autores&a=ver_lista&v=panel">Paneles</a></li>
						
						<li><a href="principal.php?c=autores&a=nuevo_autor">Nuevo autor</a></li>
						</ul></li>
						</ul>
						
						<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Libros<b class="caret"></b></a>
					<ul class="dropdown-menu">
						
						<li><a href="principal.php?c=libros&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="principal.php?c=libros&a=ver_lista&v=panel">Paneles</a></li>
						
						<li><a href="principal.php?c=libros&a=nuevo_libro">Nuevo Libro</a></li>
											
					</ul></li>
					
					<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Ejemplares<b class="caret"></b></a>
					<ul class="dropdown-menu">
						
						<li><a href="principal.php?c=ejemplares&a=ver_lista&v=tabla">Tabla</a></li>
						<li><a href="principal.php?c=ejemplares&a=ver_lista&v=panel">Paneles</a></li>
						
						<li><a href="principal.php?c=ejemplares&a=nuevo_ejemplar">Nuevo Ejemplar</a></li>
					</ul>
				</li>
				
				

				      <form class="navbar-form navbar-left"method="post" action="finalizar.php"role="search">
  
                      <button type="submit" class="btn btn-danger ">Finalizar Sesion</button>
                      </form>
			
				
		</div>
	</div>
</div>
